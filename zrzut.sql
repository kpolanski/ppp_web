-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Wersja serwera:               10.3.14-MariaDB - mariadb.org binary distribution
-- Serwer OS:                    Win64
-- HeidiSQL Wersja:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Zrzut struktury tabela ppp.bank_contrahents_mapping
CREATE TABLE IF NOT EXISTS `bank_contrahents_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `contrahent_info` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `custom_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `contrahent_info` (`contrahent_info`),
  KEY `custom_name` (`custom_name`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Eksport danych został odznaczony.

-- Zrzut struktury tabela ppp.bank_operations
CREATE TABLE IF NOT EXISTS `bank_operations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uploaded_file_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `transaction_date` date DEFAULT NULL,
  `accounting_date` date DEFAULT NULL,
  `contrahent_data` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account_number` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `details` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transaction_number` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transaction_amount` decimal(16,2) DEFAULT NULL,
  `transaction_currency` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `block_amount` decimal(16,2) DEFAULT NULL,
  `block_currency` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_amount_in_currency` decimal(16,2) DEFAULT NULL,
  `payment_currency` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `account` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance_after_transaction` decimal(16,2) DEFAULT NULL,
  `balance_after_transaction_currency` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `contrahent_data` (`contrahent_data`),
  KEY `created_at` (`created_at`),
  KEY `updated_at` (`updated_at`),
  KEY `account_number` (`account_number`),
  KEY `user_id` (`user_id`),
  KEY `title` (`title`),
  KEY `transaction_date` (`transaction_date`),
  KEY `uploaded_file_name` (`uploaded_file_name`),
  CONSTRAINT `FK_bank_operations_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Eksport danych został odznaczony.

-- Zrzut struktury tabela ppp.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Eksport danych został odznaczony.

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
