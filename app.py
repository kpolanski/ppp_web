from flask import Flask, session, request, flash, redirect, url_for, render_template
from flask_session import Session

from Storage import Storage
from bank_managment import bank_management

app = Flask(__name__)
app.secret_key = "U9JARg2CVm"
app.debug = 1
app.register_blueprint(bank_management, url_prefix='/user')


@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template('login.html')
    else:
        return redirect(url_for("bank_management.homepage"))


@app.route('/login', methods=['POST'])
def do_admin_login():
    db = Storage()
    user = db.user_credentials(request.form['username'], request.form['password'])
    if user:
        session['logged_in'] = True
        session['user_data'] = user
    else:
        flash('Błędne hasło lub użytkownik', "danger")
    return redirect(url_for('home'))


@app.route('/register', methods=['GET'])
def register_user_form():
    return render_template('register.html')


@app.route('/register', methods=['POST'])
def register_user():
    db = Storage()
    username = request.form['username']
    password = request.form['password']
    password2 = request.form['password2']
    if password == password2:
        if db.check_username(username):
            db.register_user(username, password)
        else:
            flash('Nazwa użytkownika zajęta!', 'danger')
            return redirect(url_for('register_user_form'))
    else:
        flash('Hasła są niezgodne ze sobą', 'danger')
        return redirect(url_for('register_user_form'))
    flash('Zarejestrowałeś się! Możesz się teraz zalogować i cieszyć lub smucić aplikacją', 'success')
    return redirect(url_for('home'))

@app.route("/logout")
def logout():
    session['logged_in'] = False
    return redirect(url_for('home'))


if __name__ == '__main__':
    app.run(debug=True)
