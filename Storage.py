import mysql.connector


class Storage():
    def __init__(self):
        self.conn = mysql.connector.connect(
            host="localhost",
            user="ppp",
            passwd="ppp",
            database="ppp"
        )

    def user_credentials(self, username, password):
        cursor = self.conn.cursor()
        query = "SELECT id, username FROM users WHERE username = %s AND password = MD5(%s)"
        cursor.execute(query, (username, password))
        record = cursor.fetchone()
        return record

    def register_user(self, user_login, user_password):
        cursor = self.conn.cursor()
        query = """INSERT INTO users(username, password)
            VALUES (%s, MD5(%s))"""
        result = cursor.execute(query, (user_login, user_password))
        self.conn.commit()
        return result

    def check_username(self, user_login):
        cursor = self.conn.cursor()
        query = "SELECT count(id) > 0 FROM users WHERE username = %s "
        cursor.execute(query, (user_login,))
        record = cursor.fetchone()
        return not bool(record[0])

    def remove_operations(self, user_id, file_name):
        cursor = self.conn.cursor()
        query = "DELETE FROM bank_operations WHERE user_id = %s AND uploaded_file_name = %s"
        result = cursor.execute(query, (
            user_id, file_name
        ))
        self.conn.commit()
        return result

    def insert_operations(self, user_id, file_name, operation):
        cursor = self.conn.cursor()
        query = """INSERT INTO bank_operations 
                    (user_id, uploaded_file_name, transaction_date, accounting_date, contrahent_data, title, account_number, 
                    bank_name, details, transaction_number, transaction_amount, transaction_currency, block_amount,
                    block_currency, payment_amount_in_currency, payment_currency, account, balance_after_transaction,
                    balance_after_transaction_currency) 
                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
        result = cursor.execute(query, (
            user_id, file_name, self.change_empty_string_to_null(operation[0]),
            self.change_empty_string_to_null(operation[1]),
            self.change_empty_string_to_null(operation[2]), self.change_empty_string_to_null(operation[3]),
            self.change_empty_string_to_null(operation[4]), self.change_empty_string_to_null(operation[5]),
            self.change_empty_string_to_null(operation[6]), self.change_empty_string_to_null(operation[7]),
            self.change_comma_to_dot(self.change_empty_string_to_null(operation[8])),
            self.change_empty_string_to_null(operation[9]),
            self.change_comma_to_dot(self.change_empty_string_to_null(operation[10])),
            self.change_empty_string_to_null(operation[11]),
            self.change_comma_to_dot(self.change_empty_string_to_null(operation[12])),
            self.change_empty_string_to_null(operation[13]),
            self.change_empty_string_to_null(operation[14]),
            self.change_comma_to_dot(self.change_empty_string_to_null(operation[15])),
            self.change_empty_string_to_null(operation[16])
        ))
        self.conn.commit()
        return result


    def get_operation_list(self, date_from, date_to, user_id):
        cursor = self.conn.cursor()
        query = """SELECT bo.transaction_date, bo.accounting_date, COALESCE(bcm.custom_name, bo.contrahent_data), bo.details, 
                bo.title, coalesce(bo.transaction_amount, bo.block_amount)
            FROM bank_operations bo
            LEFT JOIN bank_contrahents_mapping bcm
            ON bo.contrahent_data like bcm.contrahent_info and bcm.user_id = %s
            WHERE bo.transaction_date BETWEEN %s AND %s
            AND bo.user_id = %s"""
        cursor.execute(query, (user_id, date_from, date_to, user_id))
        records = cursor.fetchall()
        return records


    def contrahent_mapping_list(self, user_id):
        cursor = self.conn.cursor()
        query = "SELECT * FROM bank_contrahents_mapping WHERE user_id = %s "
        cursor.execute(query, (user_id,))
        records = cursor.fetchall()
        return records

    def insert_or_update_mapping(self, user_id, mapping_id, contrahent_name, mapping_name):
        cursor = self.conn.cursor()
        query = """INSERT INTO bank_contrahents_mapping(contrahent_info, custom_name, id, user_id)
            VALUES (%s, %s, %s, %s)
            ON DUPLICATE KEY UPDATE contrahent_info=%s, custom_name=%s"""
        result = cursor.execute(query, (contrahent_name, mapping_name, mapping_id, user_id, contrahent_name, mapping_name))
        self.conn.commit()
        return result

    def remove_mapping(self, user_id, mapping_id):
        cursor = self.conn.cursor()
        query = "DELETE FROM bank_contrahents_mapping WHERE id = %s AND user_id = %s"
        result = cursor.execute(query, (mapping_id, user_id))
        self.conn.commit()
        return result

    def change_comma_to_dot(self, val):
        if val is not None:
            return val.replace(',', '.')
        return val

    def change_empty_string_to_null(self, val):
        if val == '':
            return None
        return val.strip()
