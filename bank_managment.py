import codecs
import csv
import datetime

from flask import Blueprint, render_template, session, redirect, url_for, request, flash

from dateutil.parser import parse

from Storage import Storage

bank_management = Blueprint('bank_management', __name__, template_folder='templates/bank_managment')
db = Storage()


def before_request_func():
    if 'logged_in' in session and session['logged_in'] is False:
        return redirect(url_for('home'))


@bank_management.route('/')
def homepage():
    return render_template('homepage.html')


@bank_management.route('/list_of_operations')
def list_of_operations():
    date_from = request.args.get('date_from') or datetime.date.today().strftime("%Y-%m-01")
    date_to = request.args.get('date_to') or datetime.date.today().strftime("%Y-%m-%d")
    user_id = session.get('user_data')[0]
    options = {"date_from": date_from, "date_to": date_to}
    records = db.get_operation_list(date_from, date_to, user_id)
    return render_template('operations/list_of_operations.html', records=records, form_options=options)


@bank_management.route('/upload_new_bank_balance', methods=['GET'])
def upload_new_bank_balance():
    return render_template('operations/upload_new_bank_balance.html')


@bank_management.route('/upload_new_bank_balance_post', methods=['POST'])
def upload_new_bank_balance_post():
    flask_file = request.files['fileupload']
    if not flask_file:
        flash('Wrzuć plik CSV', 'warning')
        return redirect(url_for('bank_management.upload_new_bank_balance_post'))
    stream = codecs.iterdecode(flask_file.stream, 'utf-8', 'ignore')
    user_id = session.get('user_data')[0]
    db.remove_operations(user_id, flask_file.filename)
    for row in csv.reader(stream, dialect=csv.excel, delimiter=';'):
        if row:
            if is_date(row[0]):
                db.insert_operations(session.get('user_data')[0], flask_file.filename, row)
    flash('Zaimportowano Twoje operacje', 'success')
    return redirect(url_for('bank_management.homepage'))


@bank_management.route('/contrahent_mappings', methods=['GET'])
def contrahent_mappings():
    user_id = session.get('user_data')[0]
    list = db.contrahent_mapping_list(user_id)
    return render_template('contrahents/contrahent_mappings.html', list=list)


@bank_management.route('/contrahent_mapping_modify', methods=['POST'])
def modify_mapping():
    mapping_id = request.form['mapping_id']
    contrahent_name = request.form['operation_contrahent_name']
    maping_name = request.form['operation_mapping_name']
    user_id = session.get('user_data')[0]
    db.insert_or_update_mapping(user_id, mapping_id, contrahent_name, maping_name)
    flash('Dodano nowe mapowanie kontrahenta', 'success')
    return redirect(url_for('bank_management.contrahent_mappings'))

@bank_management.route('/contrahent_mapping_remove', methods=['POST'])
def remove_mapping():
    mapping_id = request.form['mapping_id']
    user_id = session.get('user_data')[0]
    db.remove_mapping(user_id, mapping_id)
    flash('Usunięto mapowanie', 'success')
    return redirect(url_for('bank_management.contrahent_mappings'))

bank_management.before_request(before_request_func)


def is_date(string, fuzzy=False):
    try:
        parse(string, fuzzy=fuzzy)
        return True
    except ValueError:
        return False